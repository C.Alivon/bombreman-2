// Compiler les fichiers d'entrée
#include <stdbool.h> // bool, true, false
#include <stdlib.h>  // rand
#include <stdio.h>   // printf
#include <limits.h>  // INT_MAX

// fichier d'en-tête du programme
#include "bomberman.h"

#define LMAX 20 // longueur maximale de la liste

// structures
struct LISTE
{
  char t[LMAX];
  int longueur;
}; // structure permettant d'avoir le type liste
typedef struct LISTE TListe;

// déclarations globales
extern const char BOMBERMAN;      // caractère ascii utilisé pour bomberman
extern const char WALL;           // caractère ascii utilisé pour les murs
extern const char BREAKABLE_WALL; // caractère ascii utilisé pour les murs cassables
extern const char PATH;           // caractère ascii utilisé pour le chemin
extern const char EXIT;           // caractère ascii utilisé pour la sortie
extern const char BOMB;           // caractère ascii utilisé pour une bombe
extern const char BOMB_BONUS;     // caractère ascii utilisé pour les bonus de bombe
extern const char FLAME_BONUS;    // caractère ascii utilisé pour le bonus flamme
extern const char FLAME_ENEMY;    // caractère ascii utilisé pour les ennemis flammes
extern const char GHOST_ENEMY;    // caractère ascii utilisé pour les ennemis fantômes

extern const int BOMB_DELAY;           // temps avant qu'une bombe explose
extern const int BREAKABLE_WALL_SCORE; // récompense pour avoir fait exploser un mur cassable
extern const int FLAME_ENEMY_SCORE;    // récompense pour avoir fait exploser un ennemi flamme
extern const int GHOST_ENEMY_SCORE;    // récompense pour avoir fait exploser un ennemi fantôme
extern const int BOMB_BONUS_SCORE;     // recompense pour un bonus de bombe, qui augmente de 1 le nombre de bombes qui peuvent être posées durant le même temps
extern const int FLAME_BONUS_SCORE;    //  recompense pour un bonus flamme, qui augmente de 1 la distance d'explosion des bombes
extern const int VIEW_DISTANCE;        // champ de vision de bomberman

extern bool DEBUG; // indique si le jeu a été lancé en mode DEBUG

const char *binome = "Alivon Clémentine"; // nom de l'étudiant.e

// prototypes des fonctions/procédures locales
TListe creation(); 
TListe *trouver_plus_petite_longueur(TListe *, TListe *, TListe *, TListe *);
void append(TListe *, char);
void printAction(action);
void printBoolean(bool);
bool breakOrNot(tree);
bool bombOrNot(tree);
bool enemyHere(tree);
bool bombInTree(tree);
bool breakFound(tree, TListe *);
int findEscapePath(tree, int length);
bool exitFound(tree, TListe *);

/*
  Fonction creation :
  Cette fonction permet de créer une variable de type liste.
  Elle retourne l, une liste vide.
*/
TListe creation()
{
  TListe l;
  l.longueur = 0;
  return l;
}
/*
  Procédure trouver_plus_petite_longueur :
  Cette procédure permet de trouver quelle est la liste la plus petite. 
  Elle prend en entrée quatres pointeurs sur listes et retourne la liste la plus petite.
*/
TListe *trouver_plus_petite_longueur(TListe *a, TListe *b, TListe *c, TListe *d)
{
  TListe *res = a;
  int min_longueur = a->longueur;

  if (b->longueur < min_longueur)
  {
    res = b;
    min_longueur = b->longueur;
  }

  if (c->longueur < min_longueur)
  {
    res = c;
    min_longueur = c->longueur;
  }

  if (d->longueur < min_longueur)
  {
    res = d;
    min_longueur = d->longueur;
  }

  return res;
}

/*
  Procédure append :
  Cette procédure permet d'implémenter une valeur à une liste donnée.
  Elle prend en entrée la liste l à implémenter ainsi que l'élément à implémenter (ici un caractère)
*/

void append(TListe *l, char elt)
{
  if (l->longueur == LMAX)
    return;                    // éviter le déborderment de la liste
  l->t[(l->longueur)++] = elt; // on met la valeur elt au dernier emplacement de la liste
}

/*
  Procédure afficher_liste :
  Cette procédure permet d'afficher une liste.
  Elle prend en entrée la liste à afficher.
  Elle a été nécessaire pour le dubuggage du programme
*/

void afficher_liste(TListe l)
{
  int longueur = l.longueur;
  printf("[");
  for (int i = 0; i < longueur; i++)
  {
    printf("%c, ", l.t[i]);
  }
  printf("]\n");
}

/*
  Fonction bomberman :
  Cette fonction sélectionne aléatoirement un déplacement valide pour BOMBERMAN en fonction de sa position actuelle sur la carte de jeu.
  Elle prend en argument l'arbre map qui fournit les types des blocs environnants
 */
action bomberman(
    tree map,            // arbre de 4 caractères modélisant un sous-ensmble de la carte
    action last_action,  // dernière action effectuée, -1 au départ
    int remaining_bombs, // nombre de bombes
    int explosion_range  // distance d'explosion des bombes
)
{
  action a; // action à choisir et à retourner

  bool ok = false; // ok vaudra true dès que l'action aléatoirement sélectionnée sera valide

  do
  {
    TListe sortieN = creation();  // liste contenant le chemin vers la sortie, si cette dernière a été trouvée en partant vers le nord
    TListe sortieS = creation();  // liste contenant le chemin vers la sortie, si cette dernière a été trouvée en partant vers le sud
    TListe sortieW = creation();  // liste contenant le chemin vers la sortie, si cette dernière a été trouvée en partant vers l'ouest
    TListe sortieE = creation();  // liste contenant le chemin vers la sortie, si cette dernière a été trouvée en partant vers l'est

    a = rand() % 5; // sélectionne aléatoirement une action : 0=BOMBING, 1=NORTH
    // on enlève au joueur la possibilité de poser une bombe s'il n'y a pas de mur cassable
    while (a == BOMBING)
    {
      a = rand() % 5;
    }

    /*si un mur cassable est sur une des 4 cases entourant le joueur, on pose une bombe,
    sinon on cherche le mur cassable le plus proche*/

    if (remaining_bombs != 0 && (breakOrNot(map) || enemyHere(map)))
    {
      a = BOMBING;
    }

    // si une bombe est sur le plateau (posée ou dans le champ de vision) on cherche à s'éloigner au maximum de la bombe
    if (last_action == BOMBING || bombInTree(map))
    {

      // on cherche le chemin le plus long à travers toutes les directions

      int escapeN = findEscapePath(map->n, 0);
      int escapeS = findEscapePath(map->s, 0);
      int escapeE = findEscapePath(map->e, 0);
      int escapeW = findEscapePath(map->w, 0);
      // on sélectionne le chemin le plus long avec un algorithme de recherche du maximum
      int max_longueur = INT_MIN;

      if (escapeN > max_longueur)
      {
        max_longueur = escapeN;
        a = NORTH;
      }
      if (escapeS > max_longueur)
      {
        max_longueur = escapeS;
        a = SOUTH;
      }
      if (escapeE > max_longueur)
      {
        max_longueur = escapeE;
        a = EAST;
      }
      if (escapeW > max_longueur)
      {
        max_longueur = escapeW;
        a = WEST;
      }
    }

    /* si la sortie est dans le champ de vision, alors on récupère la dernière valeur de la liste contenant le chemin
       et cette valeur sera la prochaine action du joueur*/
    bool isSortieN = exitFound(map->n, &sortieN);
    bool isSortieS = exitFound(map->s, &sortieS);
    bool isSortieE = exitFound(map->e, &sortieE);
    bool isSortieW = exitFound(map->w, &sortieW);

    if (isSortieN || isSortieS || isSortieE || isSortieW)
    {
    // on sélectionne le chemin le plus court avec un algorithme de recherche du minimum 

      int min_longueur = INT_MAX;

      if (isSortieN && sortieN.longueur < min_longueur)
      {

        min_longueur = sortieN.longueur;
        a = NORTH;
      }
      if (isSortieS && sortieS.longueur < min_longueur)
      {

        min_longueur = sortieS.longueur;
        a = SOUTH;
      }
      if (isSortieE && sortieE.longueur < min_longueur)
      {
  
        min_longueur = sortieE.longueur;
        a = EAST;
      }
      if (isSortieW && sortieW.longueur < min_longueur)
      {

        min_longueur = sortieW.longueur;
        a = WEST;
      }

    };


    if (DEBUG)
    { // affiche l'action aléatoirement sélectionnée, uniquement en mode DEBUG
      printf("Candidate action is: ");
      printAction(a);
      printf("\n");
    }

    switch (a)
    { // vérifie si l'action sélectionnée aléatoirement est valide, c'est-à-dire si ses préconditions sont satisfaites.
    case BOMBING:
      if (remaining_bombs > 0)
        ok = true;
      break;
    case NORTH:
      if ((map->n)->c != WALL && (map->n)->c != BREAKABLE_WALL && (map->n)->c != BOMB)
        ok = true;
      break;
    case EAST:
      if ((map->e)->c != WALL && (map->e)->c != BREAKABLE_WALL && (map->e)->c != BOMB)
        ok = true;
      break;
    case SOUTH:
      if ((map->s)->c != WALL && (map->s)->c != BREAKABLE_WALL && (map->s)->c != BOMB)
        ok = true;
      break;
    case WEST:
      if ((map->w)->c != WALL && (map->w)->c != BREAKABLE_WALL && (map->w)->c != BOMB)
        ok = true;
      break;
    }

    if (DEBUG)
    { // affiche si l'action sélectionnée aléatoirement est valide, uniquement en mode DEBUG.
      printf("Is this candidate action valide? ");
      printBoolean(ok);
      printf("\n");
    }
  } while (!ok);

  return a; // Réponse au moteur de jeu.
}

/*
  Procédure printAction :
  Cette procédure affiche le nom de l'action d'entrée à l'écran.
 */
void printAction(action a)
{
  switch (a)
  {
  case BOMBING:
    printf("BOMBING");
    break;
  case NORTH:
    printf("NORTH");
    break;
  case EAST:
    printf("EAST");
    break;
  case SOUTH:
    printf("SOUTH");
    break;
  case WEST:
    printf("WEST");
    break;
  }
}

/*
  Procédure printBoolean :
  Cette procédure affiche la valeur booléenne d'entrée à l'écran.
 */
void printBoolean(bool b)
{
  if (b == true)
  {
    printf("true");
  }
  else
  {
    printf("false");
  }
}
/*
  Procédure bombOrNot :
  Cette procédure renvoie 'true' s'il y a une bombe dans les 4 cases entourant le joueur et 'false'
  s'il n'y en a pas.
  Elle prend en argument l'arbre map, qui fournit les types des blocs environnants.
*/
bool bombOrNot(tree map)
{
  bool result = false;
  if (
      ((map->n != NULL) && ((map->n)->c == BOMB)) ||
      ((map->e != NULL) && ((map->e)->c == BOMB)) ||
      ((map->s != NULL) && ((map->s)->c == BOMB)) ||
      ((map->w != NULL) && ((map->w)->c == BOMB)))
  { // on teste les sous-arbres pour vérifier si une bombe est présente ou non
    result = true;
  }
  return result;
}
/*
  Procédure exitFound :
  Cette procédure renvoie 'true' si la sortie est dans le champ de vision et 'false' si elle n'y est pas. De plus,
  elle implémente dans une liste les directions à suivre pour se diriger vers la sortie.
  La procédure est récursive et prend en argument l'arbre map, qui fournit les types des blocs environnants ainsi qu'une liste qui
  contiendra les directions à suivre pour aller jusqu'à la sortie.
*/
bool exitFound(tree map, TListe *pathToExit)
{
  // si l'arbre est vide, on retourne 'false'
  if (map == NULL)
  {
    return false;
    // sinon, si la valeur sur laquelle le joueur est est la sortie, on ajoute le caractère 'E' à la liste et on retourne 'true'
    // pour signifier qu'on a trouvé la sortie.
  }
  else if ((map->c) == EXIT)
  {
    append(pathToExit, EXIT);
    return true;
  }

  // si le chemin vers la sortie nécéssite de passer par le nord, on ajoute le caractère 'n' à la liste pathToExit
  if (exitFound((map->n), pathToExit))
  {
    append(pathToExit, 'n');
    return true;
  }
  // si le chemin vers la sortie nécéssite de passer par le sud, on ajoute le caractère 's' à la liste pathToExit
  if (exitFound(map->s, pathToExit))
  {
    append(pathToExit, 's');
    return true;
  }
  // si le chemin vers la sortie nécéssite de passer par l'est, on ajoute le caractère 'e' à la liste pathToExit
  if (exitFound(map->e, pathToExit))
  {
    append(pathToExit, 'e');
    return true;
  }
  // si le chemin vers la sortie nécéssite de passer par l'ouest, on ajoute le caractère 'w' à la liste pathToExit
  if (exitFound(map->w, pathToExit))
  {
    append(pathToExit, 'w');
    return true;
  }
  // sinon, cela signifie que la sortie n'a pas été trouvée, alors on retourne 'false'
  return false;
}

/*
  Procédure findEscapePath :
  Cette procédure renvoie la longueur du plus grand chemin que l'on peut emprunter afin de fuire une bombe par exemple.
  La procédure est récursive et prend en argument l'arbre map, qui fournit les types des blocs environnants ainsi que la
  longueur du chemin trouvé lors du précédent appel de la fonction.
*/
int findEscapePath(tree map, int length)
{
  // Si on atteint la fin de l'arbre on renvoi la dernière longueur obtenue
  if (map == NULL)
  {
    return length;
  }

  // On initialise la longueur maximal que l'on a atteint
  int max_length = length;

  // Si l'élément courant de l'arbre contient un bloc sur lequel on peut marcher,
  // c'est que l'on peut continue de chercher un chemin plus long donc on va chercher
  // la longueur maximum parmis les 4 directions possibles (Nord, Sud, Est, Ouest)
  if (map->c != WALL || map->c != BREAKABLE_WALL || map->c != BOMB)
  {
    // Le chemin maximum est la longueur trouvé à l'appel précédent plus le bloc actuel
    max_length++;

    // On récupère la longueur maximal que l'on peut atteindre par le nord
    int current_length = findEscapePath(map->n, max_length);
    // Si cette longueur est plus grande que celle précédemment trouvé on remplace
    // l'ancienne longueur par la longueur du chemin vers le nord
    if (current_length > max_length)
    {
      max_length = current_length;
    }

    // On repète cette opération pour le sud
    current_length = findEscapePath(map->s, max_length);
    if (current_length > max_length)
    {
      max_length = current_length;
    }

    // On repète cette opération pour l'est
    current_length = findEscapePath(map->e, max_length);
    if (current_length > max_length)
    {
      max_length = current_length;
    }

    // On repète cette opération pour l'ouest
    current_length = findEscapePath(map->w, max_length);
    if (current_length > max_length)
    {
      max_length = current_length;
    }
  }

  // A la fin on retourne la nouvelle longueur trouvé
  return max_length;
}

/*
  Procédure breakFound :
  De la même manière que exitFound, cette procédure renvoie 'true' si un mur cassable est dans le champ de vision
  et 'false' s'il n'y en a pas. De plus, elle implémente dans une liste les directions à suivre pour se diriger vers le bloc visé.
  La procédure est récursive et prend en argument l'arbre map, qui fournit les types des blocs environnants ainsi qu'une liste qui
  contiendra les directions.
*/
bool breakFound(tree map, TListe *pathToBreak)
{
  // si l'arbre est vide, on retourne 'false'
  if (map == NULL || map->c == BOMB)
  {
    return false;
    // sinon, si la valeur sur laquelle le joueur est est la sortie, on ajoute le caractère 'E' à la liste et on retourne 'true'
    // pour signifier qu'on a trouvé la sortie.
  }
  else if ((map->c) == BREAKABLE_WALL)
  {
    append(pathToBreak, BREAKABLE_WALL);
    return true;
  }


  // si le chemin vers le bloc visé nécéssite de passer par le nord, on ajoute le caractère 'n' à la liste pathToBreak
  if (breakFound((map->n), pathToBreak))
  {
    append(pathToBreak, 'n');
    return true;
  }
  // si le chemin vers le bloc visé nécéssite de passer par le sud, on ajoute le caractère 's' à la liste pathToBreak
  if (breakFound(map->s, pathToBreak))
  {
    append(pathToBreak, 's');
    return true;
  }
  // si le chemin vers le bloc visé nécéssite de passer par l'est, on ajoute le caractère 'e' à la liste pathToBreak
  if (breakFound(map->e, pathToBreak))
  {
    append(pathToBreak, 'e');
    return true;
  }
  // si le chemin vers le bloc visé nécéssite de passer par l'ouest, on ajoute le caractère 'w' à la liste pathToBreak
  if (breakFound(map->w, pathToBreak))
  {
    append(pathToBreak, 'w');
    return true;
  }
  // sinon, cela signifie qu'aucun mur cassable n'a été trouvée, alors on retourne 'false'
  return false;
}

/*
  Procédure breakOrNot :
  Cette procédure renvoie 'true' si un mur cassable est dans les 4 cases entourant le joueur et 'false'
  s'il n'y en a pas.
  Elle prend en argument l'arbre map, qui fournit les types des blocs environants.
*/
bool breakOrNot(tree map)
{
  return (
      ((map->n != NULL) && (map->n)->c == BREAKABLE_WALL) ||
      ((map->e != NULL) && (map->e)->c == BREAKABLE_WALL) ||
      ((map->s != NULL) && (map->s)->c == BREAKABLE_WALL) ||
      ((map->w != NULL) && (map->w)->c == BREAKABLE_WALL));
}

bool enemyHere(tree map)
{
  return 
      ((map->n != NULL) && ((map->n)->c == GHOST_ENEMY || (map->n)->c == FLAME_ENEMY)) ||
      ((map->s != NULL) && ((map->s)->c == GHOST_ENEMY || (map->s)->c == FLAME_ENEMY)) ||
      ((map->e != NULL) && ((map->e)->c == GHOST_ENEMY || (map->e)->c == FLAME_ENEMY)) ||
      ((map->w != NULL) && ((map->w)->c == GHOST_ENEMY || (map->w)->c == FLAME_ENEMY));      
}

/*

  Procédure bombInTree :
  Recherche dans l'arbre du champ de vision si une bombe  est présente.

*/

bool bombInTree(tree map)
{
  // si l'arbre est vide
  if (map == NULL)
  {
    return false;
  }
  else if (map->c == BOMB)
  {
    // si on trouve
    return true;
  }

  return (bombInTree(map->n) || bombInTree(map->s) || bombInTree(map->e) || bombInTree(map->w));
}